%%%-------------------------------------------------------------------
%%% @author benoitc
%%% @copyright (C) 2017, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 03. Nov 2017 13:08
%%%-------------------------------------------------------------------
-module(memstore_SUITE).
-author("benoitc").
%% API
%% API
-export([
  all/0,
  init_per_suite/1,
  end_per_suite/1,
  init_per_testcase/2,
  end_per_testcase/2
]).
-export([
  t_open_close/1,
  t_basic/1,
  t_write_batch/1,
  t_snapshot/1,
  t_version/1,
  t_iterator/1,
  t_gc/1
]).

all() ->
  [
    t_open_close,
    t_basic,
    t_write_batch,
    t_snapshot,
    t_version,
    t_iterator,
    t_gc
  ].

init_per_suite(Config) ->
  {ok, _} = application:ensure_all_started(memstore),
  Config.

end_per_suite(Config) ->
  ok = application:stop(memstore),
  Config.

init_per_testcase(_, Config) ->
  Config.
end_per_testcase(_, _Config) ->
  ok.


%% ==============================
%% tests

t_open_close(_Config) ->
  ok = memstore:open(test, []),
  ok = memstore:close(test),
  false = (ets:info(test, name) =/= undefined),
  ok = memstore:open(test, []),
  ok = memstore:close(test),
  ok = memstore:close(test).

%% @doc test the cache distribution
t_basic(_Config) ->
  ok = memstore:open(test, []),
  not_found = memstore:get(test, a),
  ok = memstore:put(test, a, b),
  {ok, b} = memstore:get(test, a),
  ok = memstore:delete(test, a),
  not_found = memstore:get(test, a),
  ok = memstore:close(test).

t_write_batch(_Config) ->
  ok = memstore:open(test, []),
  ok = memstore:write_batch(test,
    [
      {put, a, b},
      {put, c, d}
    ]
  ),
  {ok, b} = memstore:get(test, a),
  {ok, d} = memstore:get(test, c),
  ok = memstore:write_batch(test,
    [
      {delete, a},
      {delete, c}
    ]
  ),
  not_found = memstore:get(test, a),
  not_found = memstore:get(test, c),
  ok = memstore:close(test).
  

t_snapshot(_Config) ->
  ok = memstore:open(test, []),
  not_found = memstore:get(test, a),
  ok = memstore:put(test, a, b),
  {ok, b} = memstore:get(test, a),
  Snapshot = memstore:new_snapshot(test),
  1 = memstore:get_snapshot_sequence(Snapshot),
  ok = memstore:put(test, c, d),
  {ok, b} = memstore:get(test, a, [{snapshot, Snapshot}]),
  not_found = memstore:get(test, c, [{snapshot, Snapshot}]),
  {ok, d} = memstore:get(test, c),
  ok = memstore:close(test).
  

t_version(_Config) ->
  ok = memstore:open(test, []),
  not_found = memstore:get(test, a),
  ok = memstore:put(test, a, b),
  {ok, b} = memstore:get(test, a),
  ok = memstore:put(test, a, c),
  {ok, c} = memstore:get(test, a),
  ok = memstore:delete(test, a),
  not_found = memstore:get(test, a),
  ok = memstore:close(test).

t_iterator(_Config) ->
  ok = memstore:open(test, []),
  ok = memstore:put(test, <<"a">>, <<"x">>),
  ok = memstore:put(test, <<"b">>, <<"y">>),
  {ok, <<"x">>} = memstore:get(test, <<"a">>),
  {ok, I} = memstore:iterator(test, []),
  {ok, <<"a">>, <<"x">>} =  memstore:iterator_move(I, {seek, <<>>}),
  {ok, <<"b">>, <<"y">>} = memstore:iterator_move(I, next),
  {ok, <<"a">>, <<"x">>} = memstore:iterator_move(I, prev),
  ok = memstore:iterator_close(I),
  ok = memstore:close(test).

t_gc(_Config) ->
  ok = memstore:open(test, [{compaction_interval, 100}]),
  ok = memstore:put(test, a, b),
  timer:sleep(200),
  {ok, b} = memstore:get(test, a),
  ok = memstore:put(test, a, c),
  {ok, c} = memstore:get(test, a),
  timer:sleep(200),
  Tab = ets:lookup_element(test, tab, 2),
  1 = ets:info(Tab, size),
  {ok, c} = memstore:get(test, a),
  ok = memstore:close(test).